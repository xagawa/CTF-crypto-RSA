```python:
from binascii import hexlify, unhexlify

hexlify(b"Hello") 
# => b'48656c6c6f'
a = int(hexlify(b"Hello"),16)
# => 0x48656c6c6f = 310939249775

# hexはsagemathのintegerを引数に取れないので, 型変換している
# この例ではaが十分短いので, 型変換しなくてもよい

hex(int(a))
# => b'0x48656c6c6f'
hex(int(a)).strip('0x')
# => b'48656c6c6f'
unhexlify(hex(int(a)).strip('0xL'))
# => b'Hello'

pow(2,5)
# => 2^5
pow(2,5,11)
# => 2^5 mod 11

pow(8,1/3)
# => 2 (ただしこれは整数ではない)
int(pow(8,1/3))
# => 2 (これは整数)
```

* 
long integerの場合、末尾にLが付くので ``strip('0xL')`` としている
